# Crear instancias de estudiantes
from Curso import Curso
from Estudiante import Estudiante


estudiante1 = Estudiante("Miguel Chacana")
estudiante2 = Estudiante("Daniel Gonzalez")
estudiante3 = Estudiante("Carolina Tapia")

# Crear instancias/objetos de cursos
curso1 = Curso("POO")
curso2 = Curso("Movil")
curso3 = Curso("DevSecOps")

# Inscribir estudiantes en cursos
estudiante1.inscribirse_en_curso(curso1)
estudiante1.inscribirse_en_curso(curso2)
estudiante2.inscribirse_en_curso(curso1)
estudiante3.inscribirse_en_curso(curso2)
estudiante3.inscribirse_en_curso(curso3)

# Imprimir lista de estudiantes inscritos en cada curso
for curso in [curso1, curso2, curso3]:
    print(f"Estudiantes inscritos en {curso.nombre}:")
    for estudiante in curso.estudiantes:
        print(estudiante.nombre)
    print()