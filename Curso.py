class Curso:
    def __init__(self, nombre):
        self.nombre = nombre
        self.estudiantes = set()  # Usamos un conjunto para mantener un registro de los estudiantes inscritos

    def inscribir_estudiante(self, estudiante):
        self.estudiantes.add(estudiante)
        # No es necesario inscribir nuevamente al estudiante en esta clase, ya que se hace en la clase Estudiante
       