from Curso import Curso

class Estudiante:
    
    def __init__(self, nombre):
        self.nombre = nombre
        self.cursos = []  # Usamos una lista/arreglo para mantener un registro de los cursos inscritos
        # self.cursos = set()  # Usamos un conjunto para mantener un registro de los cursos inscritos

    def inscribirse_en_curso(self, curso):
        self.cursos.append(curso)
        # self.cursos.add(curso) add es el metodo para agregar si usaramos un tipo conjunto "set"
        curso.inscribir_estudiante(self)